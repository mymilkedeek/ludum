package net.mymilkedeek.ludum.twentyfour.states

import net.mymilkedeek.ludum.twentyfour.model.Context
import net.mymilkedeek.ludum.twentyfour.model.Crop
import net.mymilkedeek.ludum.twentyfour.model.Seed
import net.mymilkedeek.ludum.twentyfour.model.Species
import org.newdawn.slick.Color
import org.newdawn.slick.Input
import org.newdawn.slick.state.BasicGameState

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */
class InventoryState extends BasicGameState {
    @Override
    int getID() {
        return States.INVENTORY
    }

    @Override
    void init(org.newdawn.slick.GameContainer gameContainer, org.newdawn.slick.state.StateBasedGame stateBasedGame) {

    }

    @Override
    void render(org.newdawn.slick.GameContainer gameContainer, org.newdawn.slick.state.StateBasedGame stateBasedGame, org.newdawn.slick.Graphics graphics) {
        // info bar rendering
        graphics.setColor(Color.darkGray)
        graphics.fillRect(0, 0, 800, 35)
        graphics.setColor(Color.white)
        graphics.drawLine(0, 35, 800, 35)

        graphics.drawString("TURN $Context.world.turn", 10, 10)

        graphics.drawString("MONEY $Context.world.money", 100, 10)

        graphics.drawString("[ESC: BACK | S: SELL]", 350, 10)
        def fps = gameContainer.getFPS()
        graphics.drawString("[FPS $fps]", 700, 10)

        def y = 50
        def x = 50
        def counter = 0
        def instance = "Crop"

        Context.inventory.contents.each {
            if (it instanceof Seed)
                instance = "Seed"

            if (counter == currentSelection) {
                graphics.drawString(">", x - 20, y)
                
                Crop item = Context.inventory.contents.get(currentSelection)

                graphics.drawString(Species.name(item.species) + "(" + instance + ")", 50, 310 )
                graphics.drawString("DNA = $item.dna", 50, 340)
                graphics.drawString("Growth = $item.growTime", 50, 370)
                graphics.drawString("Seeds = $item.seeds", 50, 400)
                graphics.drawString("Height = $item.height m", 50, 430)
                graphics.drawString("Fruit size = $item.fruitSize", 50, 460)
                
                graphics.drawString("Leaf size = $item.leafSize", 270, 310)
                
                graphics.color = Color.cyan
                graphics.drawString("Market value = $item.marketPrice", 270, 370)
                graphics.color = Color.white
            }

            graphics.drawString(instance + " : " + Species.name(it.species), x, y)
            y += 30
            counter++

            if (counter % 7 == 0) {
                y = 50
                x += 270

            }
        }

        graphics.setColor(Color.darkGray)
        graphics.fillRect(0, 260, 800, 40)
        graphics.setColor(Color.white)
        graphics.drawLine(0, 260, 800, 260)
        graphics.drawLine(0, 300, 800, 300)
        
        graphics.drawString("Details Selection", 10, 270)
        graphics.drawString("Breeding Selection",600, 270)

        graphics.drawString("#1 [key : q]", 550, 310)

        graphics.drawString("#2 [key : w]", 550, 370)
        
        graphics.drawString("Breed [key : b]", 550, 460)

        if ( breed1 > -1 ) {
            graphics.drawString(Species.name(Context.inventory.contents.get(breed1).species) + " " + Context.inventory.contents.get(breed1).dna, 550, 340)
        }

        if ( breed2 > -1 ) {
            graphics.drawString(Species.name(Context.inventory.contents.get(breed2).species) + " " + Context.inventory.contents.get(breed2).dna, 550, 400)
        }
    }

    Input input

    int currentSelection = 0
    int breed1 = -1
    int breed2 = -1

    @Override
    void update(org.newdawn.slick.GameContainer gameContainer, org.newdawn.slick.state.StateBasedGame stateBasedGame, int i) {
        input = gameContainer.input

        if (input.isKeyPressed(Input.KEY_ESCAPE)) {
            stateBasedGame.enterState(States.MAINGAME)
        }

        if (input.isKeyPressed(Input.KEY_DOWN)) {
            currentSelection++
        }
        if (input.isKeyPressed(Input.KEY_UP)) {
            currentSelection--
        }
        
        if ( input.isKeyPressed(Input.KEY_RIGHT)) {
            currentSelection += 7
        }
        
        if ( input.isKeyPressed(Input.KEY_LEFT)) {
            currentSelection -= 7
        }

        if ( currentSelection < 0 ) {
            currentSelection = 0
        }

        if ( currentSelection > Context.inventory.contents.size() - 1 ) {
            currentSelection--
        }
        
        if ( input.isKeyPressed(Input.KEY_Q) ) {
            if ( breed1 == currentSelection ) {
                breed1 = -1
            } else {
                if ( currentSelection != breed2 ) {
                    if ( Context.inventory.contents.get(currentSelection) instanceof Seed ) {
                    } else {
                        breed1 = currentSelection
                    }
                }
            }
        }

        if ( input.isKeyPressed(Input.KEY_W) ) {
            if ( breed2 == currentSelection ) {
                breed2 = -1
            } else {
                if ( currentSelection != breed1 ) {
                    if ( Context.inventory.contents.get(currentSelection) instanceof Seed ) {
                    } else {
                        breed2 = currentSelection
                    }
                }
            }
        }
        
        if ( input.isKeyPressed(Input.KEY_B)) {
            if ( breed2 != -1 ) {
                if ( breed1 != -1 ) {
                    Seed seed = Context.inventory.contents.get(breed1).breed(Context.inventory.contents.get(breed2))
                    Context.inventory.contents.remove(breed1)
                    if ( breed1 < breed2 ) {
                        breed2--
                    }
                    Context.inventory.contents.remove(breed2)
                    Context.inventory.contents.add(seed)
                    breed1 = -1
                    breed2 = -1
                }
            }
        }
        
        if ( input.isKeyPressed(Input.KEY_S)) {
            Context.world.money += Context.inventory.contents.get(currentSelection).marketPrice
            Context.inventory.contents.remove(currentSelection)
        }
        
        if ( input.isKeyPressed(Input.KEY_P)) {
            if ( Context.inventory.contents.get(currentSelection) instanceof Seed ) {
                Seed seed = Context.inventory.contents.remove(currentSelection)
                Context.world.plant(seed)
            }
        }
    }
}
