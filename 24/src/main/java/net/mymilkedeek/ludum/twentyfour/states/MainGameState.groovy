package net.mymilkedeek.ludum.twentyfour.states

import net.mymilkedeek.ludum.twentyfour.listener.MyKeyListener
import net.mymilkedeek.ludum.twentyfour.listener.MyMouseListener
import net.mymilkedeek.ludum.twentyfour.model.Context
import org.newdawn.slick.Color
import org.newdawn.slick.Image
import org.newdawn.slick.Input
import org.newdawn.slick.state.BasicGameState
import javax.swing.JOptionPane
import net.mymilkedeek.ludum.twentyfour.model.Seed

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */
class MainGameState extends BasicGameState {

    @Override
    int getID() {
        return States.MAINGAME
    }

    @Override
    void init(org.newdawn.slick.GameContainer gameContainer, org.newdawn.slick.state.StateBasedGame stateBasedGame) {
        gameContainer.input.addMouseListener(new MyMouseListener())
        gameContainer.input.addKeyListener(new MyKeyListener())

        gameContainer.setMouseCursor(new Image("resources/mouse/mouse_regular.png"), 20, 20)
    }

    @Override
    void render(org.newdawn.slick.GameContainer gameContainer, org.newdawn.slick.state.StateBasedGame stateBasedGame, org.newdawn.slick.Graphics graphics) {
        // info bar rendering
        graphics.setColor(Color.darkGray)
        graphics.fillRect(0, 0, 800, 35)
        graphics.setColor(Color.white)
        graphics.drawLine(0, 35, 800, 35)

        graphics.drawString("TURN $Context.world.turn", 10, 10)
        
        graphics.drawString("MONEY $Context.world.money", 100, 10)

        graphics.drawString("Inventory ($Context.inventory.contents.size items) [key: I]", 10, 50)
        graphics.drawString("Marketplace [key: M]", 10, 80)
        graphics.drawString("Enter your own DNA [key: d] cost: 20", 10, 110)

        def fps = gameContainer.getFPS()
        graphics.drawString("[FPS $fps]", 700, 10 )
        // start scene rendering

        graphics.drawString("Farm Land", 305, 50)

        Context.world.map.render(305, 70)
        
    }

    Input input
    
    @Override
    void update(org.newdawn.slick.GameContainer gameContainer, org.newdawn.slick.state.StateBasedGame stateBasedGame, int i) {
        input = gameContainer.input
        
        if ( input.isKeyPressed(Input.KEY_I) ) {
            stateBasedGame.enterState(States.INVENTORY)
        }
        
        if ( input.isKeyPressed(Input.KEY_M)) {
            stateBasedGame.enterState(States.MARKET)
        }
        
        if ( input.isKeyPressed(Input.KEY_D)) {
            if ( Context.world.money >= 20 ) {
                String dna = JOptionPane.showInputDialog(null, "Please enter a dna code")
                if (dna == null ) {
                    dna = "caranha"
                }
                Context.world.money -= 20
                Seed seed = new Seed(dna)
                boolean entered = Context.inventory.add(seed)
                if ( ! entered ) {
                    Context.world.plant(seed)
                }
            }
        }
    }
}
