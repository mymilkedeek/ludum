package net.mymilkedeek.ludum.twentyfour

import net.mymilkedeek.ludum.twentyfour.states.IntroState
import net.mymilkedeek.ludum.twentyfour.states.States
import org.newdawn.slick.state.StateBasedGame
import net.mymilkedeek.ludum.twentyfour.states.MainGameState
import net.mymilkedeek.ludum.twentyfour.model.Context
import net.mymilkedeek.ludum.twentyfour.model.World
import org.newdawn.slick.tiled.TiledMap
import net.mymilkedeek.ludum.twentyfour.model.Inventory
import net.mymilkedeek.ludum.twentyfour.model.Crop
import net.mymilkedeek.ludum.twentyfour.states.InventoryState
import net.mymilkedeek.ludum.twentyfour.model.Seed
import net.mymilkedeek.ludum.twentyfour.states.MarketState

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */
class Game extends StateBasedGame {
    
    Game(title) {
        super(title)

        addState(new IntroState())
        addState(new MainGameState())
        addState(new InventoryState())
        addState(new MarketState())

        enterState(States.MAINGAME)
    }
    
    @Override
    void initStatesList(org.newdawn.slick.GameContainer gameContainer) {
        Context.world = new World(map: new TiledMap("resources/tiled/map.tmx"))
        Context.inventory = new Inventory()
        Context.inventory.contents.add( new Crop("ludumdare"))
        Context.inventory.contents.add( new Crop("evolution"))
        Context.inventory.contents.add( new Crop("mymilkedeek"))
        Context.inventory.contents.add( new Crop("caranha"))
        Context.inventory.contents.add( new Seed("gamedev"))
    }
}
