package net.mymilkedeek.ludum.twentyfour.model

import org.newdawn.slick.tiled.TiledMap
import org.newdawn.slick.geom.Vector2f

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */
class World {
    TiledMap map
    def turn = 1
    boolean gameEnded = false
    def money = 20
    def seedLocations = []
    def seeds = []

    void nextTurn() {
        turn++

        def toRemove = []
        def counter = 0

        seeds.each {
            it.currentGrowth++
            if (it.growTime == it.currentGrowth) {
                def name = Species.name(it.species)
                Context.inventory.add(it.grow())
                toRemove.add(counter)
            }
            counter++
        }

        if (! toRemove.isEmpty()) {
            for (i in toRemove.size() - 1..0) {
                seeds.remove(toRemove[i])
                seedLocations.remove(toRemove[i])
            }
        }

        if (turn == 100) {
            gameEnded = true
        }
    }

    void plant(Seed seed) {
        Random r = new Random()
        seeds.add(seed)
        seedLocations.add(new Vector2f(r.nextInt(480), r.nextInt(300)))
    }
}