package net.mymilkedeek.ludum.twentyfour.listener

import net.mymilkedeek.ludum.twentyfour.model.Context
import org.newdawn.slick.Input
import org.newdawn.slick.KeyListener

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */
class MyKeyListener implements KeyListener {
    @Override
    void keyPressed(int i, char c) {
        switch ( i ) {
            case Input.KEY_SPACE:
                Context.world.nextTurn()
                break
        }
    }

    @Override
    void keyReleased(int i, char c) {

    }

    @Override
    void setInput(org.newdawn.slick.Input input) {

    }

    @Override
    boolean isAcceptingInput() {
        return true
    }

    @Override
    void inputEnded() {

    }

    @Override
    void inputStarted() {

    }
}
