package net.mymilkedeek.ludum.twentyfour.model

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */
class Seed extends Crop {
    Seed(String dna) {
        super(dna)
        marketPrice = 1
    }

    int currentGrowth
    
    Crop grow() {
        return new Crop(this)
    }
}
