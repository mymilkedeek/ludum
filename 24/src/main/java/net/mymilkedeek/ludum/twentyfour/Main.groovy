package net.mymilkedeek.ludum.twentyfour

import org.newdawn.slick.AppGameContainer

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */
class Main {

    static void main(args) {
        def gameContainer = new AppGameContainer(new Game("Ludum Dare 24"))
        gameContainer.setDisplayMode(800, 500, false)
        gameContainer.showFPS = false
        gameContainer.start()
    }
}
