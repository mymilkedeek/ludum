package net.mymilkedeek.ludum.twentyfour.listener

import org.newdawn.slick.MouseListener

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */
class MyMouseListener implements MouseListener {
    MyMouseListener() {
    }
    
    @Override
    void mouseWheelMoved(int i) {

    }

    @Override
    /*
     * 0 = LMB
     * 1 = RMB
     * 2 = CMB
     */
    void mouseClicked(int button, int x, int y, int clickcount) {
    }

    @Override
    void mousePressed(int i, int i1, int i2) {

    }

    @Override
    void mouseReleased(int i, int i1, int i2) {

    }

    @Override
    void mouseMoved(int oldx, int oldy, int newx, int newy) {
    }

    @Override
    void mouseDragged(int i, int i1, int i2, int i3) {

    }

    @Override
    void setInput(org.newdawn.slick.Input input) {

    }

    @Override
    boolean isAcceptingInput() {
        return true
    }

    @Override
    void inputEnded() {

    }

    @Override
    void inputStarted() {

    }
}
