package net.mymilkedeek.ludum.twentyfour.states

import org.newdawn.slick.state.BasicGameState

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */
class IntroState extends BasicGameState {

    @Override
    int getID() {
        return States.INTRO
    }

    @Override
    void init(org.newdawn.slick.GameContainer gameContainer, org.newdawn.slick.state.StateBasedGame stateBasedGame) {
    }

    @Override
    void render(org.newdawn.slick.GameContainer gameContainer, org.newdawn.slick.state.StateBasedGame stateBasedGame, org.newdawn.slick.Graphics graphics) {
    }

    @Override
    void update(org.newdawn.slick.GameContainer gameContainer, org.newdawn.slick.state.StateBasedGame stateBasedGame, int i) {
    }
}
