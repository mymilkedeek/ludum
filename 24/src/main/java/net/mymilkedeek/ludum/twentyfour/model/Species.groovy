package net.mymilkedeek.ludum.twentyfour.model

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */
class Species {
    
    static String name(int id) {
        switch ( id ) {
            case 0:
                return "Corn"
            case 1:
                return "Broccoli"
            case 2:
                return "Apple"
            case 3:
                return "Fig"
            case 4:
                return "Lettuce"
            case 5:
                return "Lime"
            case 6:
                return "Pear"
            case 7:
                return "Potato"
            case 8:
                return "Spinach"
            case 9:
                return "Tomato"
        }
    } 
}
