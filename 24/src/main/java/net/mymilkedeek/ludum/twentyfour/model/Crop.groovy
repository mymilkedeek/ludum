package net.mymilkedeek.ludum.twentyfour.model

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */
class Crop {
    String dna
    String dnaValue

    int marketPrice

    int growTime
    int seeds
    int species
    int height
    int fruitSize
    int leafSize
    
    Crop(String dna) {
        this.dna = dna
        String dnaValueTemp = dna.hashCode()
        dnaValueTemp = dnaValueTemp.replaceAll("-", "")
        
        if (dnaValueTemp.equals("0")) {
            dnaValueTemp = "aaa".hashCode()
        }
        
        if ( dnaValueTemp.length() < 6 ) {
            while ( dnaValueTemp.length() < 6 ) {
                dnaValueTemp += "0"
            }
        }
        dnaValue = dnaValueTemp

        species = Integer.parseInt(dnaValue.substring(0, 1))
        seeds = Integer.parseInt(dnaValue.substring(1, 2))
        height = Integer.parseInt(dnaValue.substring(2, 3))
        growTime = Integer.parseInt(dnaValue.substring(3, 4))
        fruitSize = Integer.parseInt(dnaValue.substring(4, 5))
        leafSize = Integer.parseInt(dnaValue.substring(5, 6))

        growTime /= 2
        growTime++
        seeds++
        
        marketPrice = ( ( seeds * 2 ) - ( growTime ) ) * ( 2 )
    }
    
    Crop(Seed crop) {
        this.dna = crop.dna
        this.dnaValue = crop.dnaValue

        this.growTime = crop.growTime
        this.seeds = crop.seeds
        this.species = crop.species
        this.height = crop.height
        this.fruitSize = crop.fruitSize
        this.leafSize = crop.leafSize

        marketPrice = ( ( seeds * 2 ) - ( this.growTime ) + 1 ) * 2
    }
    
    Seed breed(Crop partner) {
        String a = this.dna
        String b = partner.dna
        String c = ""
        Random r = new Random()
        
        for ( i in 0..5 ) {
            boolean gen = r.nextBoolean()
            
            if ( i >= a.size() ) {
                gen = false
            }
            
            if ( gen ) {
                c += a.charAt(i)
            } else {
                if ( i >= b.size() ) {
                } else {
                    c += b.charAt(i)
                }
            }
        }
        
        new Seed(c)
    }
    
    String toString() {
        return """
        dna     : $dnaValue
        dnaStr  : $dna
        species : $species
        seeds   : $seeds
        height  : $height
        growTi  : $growTime
        fruitSi : $fruitSize
        leafSi  : $leafSize
        market  : $marketPrice
        """
    }
}