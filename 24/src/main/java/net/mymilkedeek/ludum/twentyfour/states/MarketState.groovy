package net.mymilkedeek.ludum.twentyfour.states

import net.mymilkedeek.ludum.twentyfour.model.Context
import net.mymilkedeek.ludum.twentyfour.model.Seed
import org.newdawn.slick.Color
import org.newdawn.slick.Input
import org.newdawn.slick.state.BasicGameState

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */
class MarketState extends BasicGameState {

    def selection = ["Corn", "Brocolli", "Apple", "Fig", "Lettuce", "Lime", "Pear", "Potato", "Spinach", "Tomato"]
    int currentSelection = 0
    boolean fullBag = false

    @Override
    int getID() {
        return States.MARKET
    }

    @Override
    void init(org.newdawn.slick.GameContainer gameContainer, org.newdawn.slick.state.StateBasedGame stateBasedGame) {

    }

    @Override
    void render(org.newdawn.slick.GameContainer gameContainer, org.newdawn.slick.state.StateBasedGame stateBasedGame, org.newdawn.slick.Graphics graphics) {
        // info bar rendering
        graphics.setColor(Color.darkGray)
        graphics.fillRect(0, 0, 800, 35)
        graphics.setColor(Color.white)
        graphics.drawLine(0, 35, 800, 35)

        graphics.drawString("TURN $Context.world.turn", 10, 10)

        graphics.drawString("MONEY $Context.world.money", 100, 10)

        graphics.drawString("[ESC: BACK | B: BUY]", 350, 10)

        def x = 50
        def y = 50

        selection.each {
            graphics.drawString(it, x, y)
            y += 30
        }

        y = 50
        graphics.drawString(">", 30, y + (currentSelection * 30))

        fullBag = true

        if (fullBag) {
            graphics.color = Color.red
            graphics.drawString("Your inventory is full", 550, 50)
            graphics.color = Color.white
        }

        if (Context.world.money <= 0) {
            graphics.color = Color.red
            graphics.drawString("You're out of cash", 550, 80)
            graphics.color = Color.white
        }

    }

    Input input

    @Override
    void update(org.newdawn.slick.GameContainer gameContainer, org.newdawn.slick.state.StateBasedGame stateBasedGame, int i) {
        input = gameContainer.input

        if (input.isKeyPressed(Input.KEY_DOWN)) {
            currentSelection++
        }

        if (input.isKeyPressed(Input.KEY_UP)) {
            currentSelection--
        }

        if (currentSelection < 0) {
            currentSelection = 0
        }

        if (currentSelection > selection.size() - 1) {
            currentSelection--
        }

        if (input.isKeyPressed(Input.KEY_B)) {
            if (Context.inventory.contents.size() < 21) {
                if (Context.world.money >= 5) {
                    fullBag = false

                    Random r = new Random()
                    String dna = ""

                    for (index in 0..5) {
                        dna += r.nextInt(10)
                    }

                    Seed seed = new Seed(dna)
                    seed.species = currentSelection

                    Context.inventory.add(seed)
                    Context.world.money -= 5
                }
            } else {
                fullBag = true
            }
        }

        if (input.isKeyPressed(Input.KEY_ESCAPE)) {
            stateBasedGame.enterState(States.MAINGAME)
        }
    }
}
